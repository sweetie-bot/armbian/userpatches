diff --git a/arch/arm/boot/dts/rk3288-tinker.dts b/arch/arm/boot/dts/rk3288-tinker.dts
index 5775448b..e313506f 100644
--- a/arch/arm/boot/dts/rk3288-tinker.dts
+++ b/arch/arm/boot/dts/rk3288-tinker.dts
@@ -254,6 +254,26 @@
 			turbo-mode;
 		};
 	};
+
+	thundervolt-leds {
+		/* It's a simple GPIO-based LED, using a suitable driver */
+		compatible = "gpio-leds";
+	
+		/* ThunderVolt user LED */
+		thundervolt-user {
+			label = "thundervolt:user";
+			/* Pin 7 of ThunderVolt I2C GPIO expander (registered earlier), flags = 0b0001 (LSB):
+			1 - active low;
+			0 - push-pull;
+			0 - NA;
+			0 - maintain value during sleep/low-power mode. */
+			gpios = <&thundervolt_internal 7 1>;
+			/* Turned off by default */
+			default-state = "off";
+			/* Using it as a system heartbeat indicator */
+			linux,default-trigger = "heartbeat";
+		};
+	};
 };
 
 &cpu0 {
@@ -482,6 +502,37 @@
 
 &i2c1 {
 	status = "okay";
+
+
+	/* ThunderVolt logic is based on GPIOs wired to the I2C expander */
+	thundervolt_internal: pca9554@20 {
+		status = "ok";
+		/* Chip vendor and model (from kernel module Device Tree bindings file) */
+		compatible = "nxp,pca9554";
+		/* Device address on the I2C bus */
+		reg = <0x20>;
+		/* It's a GPIO controller device */
+		gpio-controller;
+		/* Two cells type GPIO specification */
+		/* WARNING: GPIO controller won't work without this parameter */
+		#gpio-cells = <2>;
+	};
+
+	/* ThunderVolt contains bq40z60 battery controller */
+	thundervolt_supply: bq40z60@0b {
+		status = "ok";
+		/* bq40z60 is compatible with SBS battery standart */
+		compatible = "sbs,sbs-battery";
+		/* Device address on the I2C bus */
+		reg = <0x0b>;
+		/* Number of retries in case the I2C operation failed */
+		sbs,i2c-retry-count = <2>;
+		/* The number of times to try looking for new status after an external change notification */
+		sbs,poll-retry-count = <10>;
+		/* The gpio which signals battery detection and a flag specifying its polarity. */
+		sbs,battery-detect-gpios = <&gpio5 14 1>; // GP5B4
+	};
+
 };
 
 &i2c2 {
diff --git a/arch/arm/boot/dts/rk3288.dtsi b/arch/arm/boot/dts/rk3288.dtsi
index 264eee96..730d0a72 100644
--- a/arch/arm/boot/dts/rk3288.dtsi
+++ b/arch/arm/boot/dts/rk3288.dtsi
@@ -1863,14 +1863,6 @@
 				rockchip,pins = <5 12 3 &pcfg_pull_up>,
 						<5 13 3 &pcfg_pull_none>;
 			};
-
-			uart4_cts: uart4-cts {
-				rockchip,pins = <5 14 3 &pcfg_pull_up>;
-			};
-
-			uart4_rts: uart4-rts {
-				rockchip,pins = <5 15 3 &pcfg_pull_none>;
-			};
 		};
 
 		tsadc {
